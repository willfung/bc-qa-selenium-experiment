from selenium.webdriver.common.by import By
from pages.web.staff_base import StaffBasePage
from pypom import Region
from pages.web.wpadmin.v3.forms.form_fields import Poll
from selenium.webdriver.common.action_chains import ActionChains


class EditFormPage(StaffBasePage):

    URL_TEMPLATE = "/wp-admin/admin.php?page=gf_edit_forms&id={form_id}"

    _body_locator = (By.CSS_SELECTOR, "#no-fields > div.newform_notice")
    _edit_locator = (By.CSS_SELECTOR, "li.gf_form_toolbar_editor > a")
    _settings_locator = (By.CSS_SELECTOR, "li.gf_form_toolbar_settings > a")
    _settings_submenu_locator = (By.CSS_SELECTOR, "li.gf_form_toolbar_settings > div > ul > li > a")
    _entries_locator = (By.CSS_SELECTOR, "li.gf_form_toolbar_entries > a")
    _preview_locator = (By.CSS_SELECTOR, "li.gf_form_toolbar_preview > a")
    _standard_fields_heading_locator = (By.CSS_SELECTOR, "#add_standard_fields > div > div")
    _advanced_fields_heading_locator = (By.CSS_SELECTOR, "#add_advanced_fields > div > div")
    _update_locator = (By.CSS_SELECTOR, "#floatMenu > input.button.button-large.button-primary.update-form")
    _cancel_locator = (By.CSS_SELECTOR, "#floatMenu > a.button.button-large.cancel-update-form")
    _move_to_trash_locator = (By.CSS_SELECTOR, "#floatMenu > a.submitdelete")

    @property
    def body(self):
        return self.find_element(*self._body_locator)

    @property
    def edit(self):
        return self.find_element(*self._edit_locator)

    @property
    def settings(self):
        return self.find_element(*self._settings_locator)

    @property
    def form_settings(self):
        _ = self.find_elements(*self._settings_submenu_locator)
        return _[0]

    @property
    def confirmations(self):
        _ = self.find_elements(*self._settings_submenu_locator)
        return _[1]

    @property
    def notifications(self):
        _ = self.find_elements(*self._settings_submenu_locator)
        return _[2]

    @property
    def personal_data(self):
        _ = self.find_elements(*self._settings_submenu_locator)
        return _[3]

    @property
    def mailchimp(self):
        _ = self.find_elements(*self._settings_submenu_locator)
        return _[4]

    @property
    def entries(self):
        return self.find_element(*self._entries_locator)

    @property
    def preview(self):
        return self.find_element(*self._preview_locator)

    @property
    def standard_fields_heading(self):
        return self.find_element(*self._standard_fields_heading_locator)

    @property
    def standard_fields(self):
        return self.StandardFields(self)

    class StandardFields(Region):

        standard_fields_locator = (By.CSS_SELECTOR, "#add_standard_fields > ul > li > ol > li > input")

        @property
        def single_line_text(self):
            _ = self.find_elements(*self.standard_fields_locator)
            return _[0]

        @property
        def paragraph_text(self):
            _ = self.find_elements(*self.standard_fields_locator)
            return _[1]

        @property
        def drop_down(self):
            _ = self.find_elements(*self.standard_fields_locator)
            return _[2]

        @property
        def multi_select(self):
            _ = self.find_elements(*self.standard_fields_locator)
            return _[3]

        @property
        def number(self):
            _ = self.find_elements(*self.standard_fields_locator)
            return _[4]

        @property
        def checkboxes(self):
            _ = self.find_elements(*self.standard_fields_locator)
            return _[5]

        @property
        def radio_buttons(self):
            _ = self.find_elements(*self.standard_fields_locator)
            return _[6]

        @property
        def hidden(self):
            _ = self.find_elements(*self.standard_fields_locator)
            return _[7]

        @property
        def section(self):
            _ = self.find_elements(*self.standard_fields_locator)
            return _[8]

        @property
        def page(self):
            _ = self.find_elements(*self.standard_fields_locator)
            return _[9]

    @property
    def advanced_fields_heading(self):
        return self.find_element(*self._advanced_fields_heading_locator)

    @property
    def advanced_fields(self):
        return self.AdvancedFields(self)

    class AdvancedFields(Region):

        _advanced_fields_locator = (By.CSS_SELECTOR, "#add_advanced_fields > ul > li > ol > li > input")

        @property
        def name(self):
            _ = self.find_elements(*self._advanced_fields_locator)
            return _[0]

        @property
        def date(self):
            _ = self.find_elements(*self._advanced_fields_locator)
            return _[1]

        @property
        def time(self):
            _ = self.find_elements(*self._advanced_fields_locator)
            return _[2]

        @property
        def phone(self):
            _ = self.find_elements(*self._advanced_fields_locator)
            return _[3]

        @property
        def address(self):
            _ = self.find_elements(*self._advanced_fields_locator)
            return _[4]

        @property
        def website(self):
            _ = self.find_elements(*self._advanced_fields_locator)
            return _[5]

        @property
        def email(self):
            _ = self.find_elements(*self._advanced_fields_locator)
            return _[6]

        @property
        def file_upload(self):
            _ = self.find_elements(*self._advanced_fields_locator)
            return _[7]

        @property
        def captcha(self):
            _ = self.find_elements(*self._advanced_fields_locator)
            return _[8]

        @property
        def list(self):
            _ = self.find_elements(*self._advanced_fields_locator)
            return _[9]

        @property
        def consent(self):
            _ = self.find_elements(*self._advanced_fields_locator)
            return _[10]

        @property
        def poll(self):
            _ = self.find_elements(*self._advanced_fields_locator)
            return _[11]

        @property
        def quiz(self):
            _ = self.find_elements(*self._advanced_fields_locator)
            return _[12]

    @property
    def poll(self):
        return Poll(self)

    @property
    def update(self):
        return self.find_element(*self._update_locator)

    @property
    def cancel(self):
        return self.find_element(*self._cancel_locator)

    @property
    def move_to_trash(self):
        return self.find_element(*self._move_to_trash_locator)

    def hover_on_element(self, element):
        ActionChains(self.driver).move_to_element(element).perform()
