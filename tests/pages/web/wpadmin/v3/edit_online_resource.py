from selenium.webdriver.common.by import By
from pages.web.wpadmin.v3.create_new_content.create_new_online_resource import CreateNewOnlineResourcePage


class EditOnlineResourcePage(CreateNewOnlineResourcePage):

    URL_TEMPLATE = "/wp-admin/post.php"

    _update_locator = (By.CSS_SELECTOR, "input#publish")

    @property
    def update(self):
        return self.find_element(*self._update_locator)
