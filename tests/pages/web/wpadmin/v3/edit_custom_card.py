from selenium.webdriver.common.by import By

from pages.web.wpadmin.v3.create_a_new_card.create_new_custom_card import CreateNewCustomCard

class EditBlogPostPage(CreateNewCustomCard):

    URL_TEMPLATE = "/wp-admin/post.php"

    _update_locator = (By.CSS_SELECTOR, "input#publish")

    @property
    def update(self):
        return self.find_element(*self._update_locator)
