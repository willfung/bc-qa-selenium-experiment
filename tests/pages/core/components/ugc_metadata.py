from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from pypom import Region
# from pages.core.components.overlay import Overlay

class UGCMetadata(Region):

    _root_locator = (By.CLASS_NAME, "ugc_loader")
    _add_to_list_locator = (By.CSS_SELECTOR, "[data-test-id='add-to-list-dropdown-container'] > a")
    _create_a_new_list_locator = (By.CLASS_NAME, "newList")
    _existing_lists_locator = (By.CSS_SELECTOR, "a[id*='more_lists_id_']")
    _add_tags_locator = (By.CSS_SELECTOR, "[testid='link_add_tags']")
    _tag_links_locator = (By.CSS_SELECTOR, "[data-test-id='tag_link']")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._add_to_list_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def add_to_list(self):
        # This element can easily appear outside the default viewport, so lets
        # scroll to it before returning it to the caller:
        element = self.find_element(*self._add_to_list_locator)
        ActionChains(self.driver).move_to_element(element).perform()
        return element

    @property
    def create_a_new_list(self):
        return self.find_element(*self._create_a_new_list_locator)

    @property
    def existing_lists(self):
        return self.find_element(*self._existing_lists_locator)

    @property
    def add_tags(self):
        # This element can easily appear outside the default viewport, so lets
        # scroll to it before returning it to the caller:
        if self.driver.capabilities['browserName'] == 'firefox':
            self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        element = self.find_element(*self._add_tags_locator)
        ActionChains(self.driver).move_to_element(element).perform()
        return element

    @property
    def tag_links(self):
        return self.find_elements(*self._tag_links_locator)

    # @property
    # def overlay(self):
    #     return Overlay(self)
