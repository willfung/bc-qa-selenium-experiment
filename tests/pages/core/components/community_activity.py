from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pypom import Region

class CommunityActivity(Region):

    _root_locator = (By.ID, "communityContent") # testid="ugc_tabs"
    _comment_tab_locator = (By.CSS_SELECTOR, "[testid='link_comment']")
    _comment_locator = (By.CSS_SELECTOR, "div[id='tab_content_comment'] > div[data-js='ugc_container']")
    _add_comment_locator = (By.CSS_SELECTOR, "[id*='add_comment_'")

    @property
    def comment(self):
        return self.Comment(self)

    class Comment(Region):
        _ratings_locator = (By.CLASS_NAME, " rating_icons")
        _date_locator = (By.CLASS_NAME, "date")
        _user_locator = (By.CLASS_NAME, "username")
        _content_locator = (By.CLASS_NAME, "parts ")
        _view_all_comments_locator = (By.CSS_SELECTOR, "div[id='tab_content_comment'] a.view_all_links")
        _permalink_icon_locator = (By.CSS_SELECTOR, "[id*='permalink_'")

        @property
        def is_ratings_displayed(self):
            try:
                return self.find_element(*self._ratings_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def ratings(self):
            stars = (self.find_element(*self._ratings_locator).get_attribute("style"))
            values = {
                "width: 100%;": "5 Stars",
                "width: 90%;": "4.5 Stars",
                "width: 80%;": "4 Stars",
                "width: 70%;": "3.5 Stars",
                "width: 60%;": "3 Stars",
                "width: 50%;": "2.5 Stars",
                "width: 40%;": "2 Stars",
                "width: 30%;": "1.5 Stars",
                "width: 20%;": "1 Star",
                "width: 10%;": "0.5 Star"
            }
            return values.get(stars, "Unknown ratings value")

        @property
        def date(self):
            return self.find_element(*self._date_locator)

        @property
        def user(self):
            return self.find_element(*self._user_locator)

        @property
        def content(self):
            return self.find_element(*self._content_locator)

        @property
        def is_view_all_comments_displayed(self):
            try:
                return self.find_element(*self._view_all_comments_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def view_all_comments(self):
            return self.find_element(*self._view_all_comments_locator)

        @property
        def permalink_icon(self):
            return self.find_element(*self._permalink_icon_locator)

    @property
    def comments(self):
        return [self.Comment(self, element) for element in self.find_elements(*self._comment_locator)]

    @property
    def add_comment(self):
        return self.find_element(*self._add_comment_locator)

    @property
    def is_add_comment_displayed(self):
        try:
            return self.find_element(*self._add_comment_locator).is_displayed()
        except NoSuchElementException:
            return False        
