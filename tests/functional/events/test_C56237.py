import pytest
import allure
# noinspection PyUnresolvedReferences
import sure
from mimesis import Text
from pages.core.home import HomePage
from pages.events.events_admin import AdminEventPage
from pages.events.home_page_events import EventsHome
from pages.events.manage_registration import ManageRegistration
from selenium.webdriver import ActionChains


@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C56237 - Event Creation with Registration that has 10+ capacity")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/56237", "TestRail")
class TestC56235:
    def test_c56235(self):

        self.base_url = "http://chipublib.local.bibliocommons.com/events/"

        # log in
        home_page = HomePage(self.driver, self.base_url).open()
        home_page_events = EventsHome(self.driver)
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)

        # navigate to events page
        events_admin = AdminEventPage(self.driver, self.base_url+"admin")
        events_admin.admin_link.click()
        events_admin.wait.until(lambda s: events_admin.is_previous_page_icon_displayed)

        # Create events
        events_admin.create_event_button.click()
        events_admin.wait.until(lambda s: events_admin.is_title_textbox_displayed)
        title_name = "py_test_C56235"

        events_admin.title_textbox.send_keys(title_name)
        events_admin.type_dropdown[0].click()
        events_admin.wait.until(lambda s: events_admin.is_type_dropdown_business_displayed)
        events_admin.type_dropdown_business.click()
        events_admin.wait.until(lambda s: events_admin.is_type_audience_dropdown_displayed)
        events_admin.audience_dropdown.click()
        events_admin.wait.until(lambda s: events_admin.is_type_audience_dropdown_families_displayed)
        events_admin.type_audience_dropdown_families.click()
        events_admin.wait.until(lambda s: events_admin.is_description_field_displayed)
        description = Text('en').text(quantity=3)
        events_admin.description_field.send_keys(description)
        events_admin.time_selector.click()
        events_admin.wait.until(lambda s: events_admin.start_time[80].is_displayed())
        events_admin.start_time[80].click()

        capacity = 10
        events_admin.registration_required_radio_buttons[1].click()
        events_admin.event_capacity_text_box.send_keys(capacity)

        # location and publish
        events_admin.wait.until(lambda s: events_admin.is_location_dropdown_displayed)
        events_admin.location_dropdown.click()
        events_admin.wait.until(lambda s: events_admin.is_location_dropdown_archer_height_displayed)
        events_admin.location_dropdown_archer.click()

        events_admin.wait.until(lambda s: events_admin.save_and_publish.is_enabled())
        events_admin.save_and_publish.click()
        events_admin.wait.until(lambda s: events_admin.is_left_chevron_button_displayed)

        home_page.open()
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)

        if home_page.header.is_collapsible_search_trigger_displayed:
            home_page.header.collapsible_search_trigger.click()
        home_page_events.search_textbox.send_keys(title_name)
        home_page_events.search_textbox_icon.click()
        home_page_events.wait.until(lambda s: events_admin.is_event_name_last_word_displayed)
        home_page_events.registration_status[1].text.should.equal(f"{capacity} seat(s) remaining")
        events_admin.event_name_last_word.text.should.equal(title_name)
        events_admin.event_name_last_word.click()
        home_page_events.wait.until(lambda s: home_page_events.is_register_button_displayed)
        home_page_events.seats_remaining.text.should.equal(f"{capacity} seats remaining")
        home_page_events.register_button.click()
        home_page_events.wait.until(lambda s: home_page_events.is_attendees_dropdown_displayed)
        home_page_events.attendees_dropdown.click()
        home_page_events.attendees_dropdown_selection[9].text.should.equal(f'{capacity}')
        home_page_events.attendees_dropdown_selection[9].click()
        home_page_events.never_mind_go_back[1].click()

        home_page_events.wait.until(lambda s: home_page_events.is_close_button_displayed)
        home_page_events.event_detail_close_button.click()

        events_admin.wait.until(lambda s: events_admin.is_admin_link_displayed)
        home_page_events.wait.until(lambda s: home_page_events.is_clear_filters_search_result_link_displayed)
        events_admin.open()
        events_admin.wait.until(lambda s: events_admin.is_admin_published_tab_displayed)
        events_admin.admin_published_tab.click()
        events_admin.wait.until(lambda s: events_admin.is_admin_events_search_textbox_displayed)

        events_admin.retry_search_until_results_appear(title_name)
        events_admin.admin_event_action_links[4].is_displayed.should.be.true

        admin_event_title = events_admin.admin_event_title
        self.driver.execute_script('arguments[0].scrollIntoView(true);', admin_event_title)
        manage_registration_button = events_admin.admin_event_action_links[4]
        ActionChains(self.driver).move_to_element(manage_registration_button).perform()
        events_admin.admin_event_action_links[4].click()

        manage_registration = ManageRegistration(self.driver)
        manage_registration.wait.until(lambda s: manage_registration.is_register_user_displayed)
        manage_registration.registration_tabs[-1].is_displayed().should.be.true
        manage_registration.registration_tabs[-2].is_displayed().should.be.true
        manage_registration.is_register_user_displayed.should.be.true

        manage_registration.number_of_spots_reserved.text.should.equal(f"0 of {capacity} spots reserved")
        manage_registration.register_user.click()
        manage_registration.wait.until(lambda s: manage_registration.register_without_library_card[1].is_displayed())
        manage_registration.register_without_library_card[1].click()
        manage_registration.wait.until(lambda s: manage_registration.is_attendees_dropdown_displayed)
        manage_registration.attendees_dropdown.click()
        manage_registration.attendees_dropdown_selection[9].text.should.equal(f'{capacity}')
        manage_registration.attendees_dropdown_selection[9].click()
        manage_registration.wait.until(lambda s: manage_registration.is_never_mind_displayed)
        manage_registration.never_mind.click()
        manage_registration.wait.until(lambda s: manage_registration.is_register_user_displayed)
        manage_registration.wait.until(lambda s: manage_registration.registration_tabs[-1].is_displayed())
        manage_registration.back_to_event_listing.click()

        events_admin.wait.until(lambda s: events_admin.is_admin_published_tab_displayed)
        admin_event_title = events_admin.admin_event_title
        self.driver.execute_script('arguments[0].scrollIntoView(true);', admin_event_title)

        delete_button = events_admin.admin_event_action_links[7]
        ActionChains(self.driver).move_to_element(delete_button).perform()
        events_admin.wait.until(lambda s: events_admin.admin_event_action_links[7].is_displayed())
        events_admin.admin_event_action_links[7].click()
        events_admin.wait.until(lambda s: events_admin.is_confirm_delete_button_displayed)

        events_admin.confirm_delete_button.click()
        events_admin.wait.until(lambda s: events_admin.is_close_delete_overlay_button_displayed)
        events_admin.close_delete_overlay_button.click()
        events_admin.wait.until(lambda s: events_admin.no_results_displayed)
        events_admin.assert_is_no_results_displayed.should.be.true
