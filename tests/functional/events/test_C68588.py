import pytest
import allure
import sys
sys.path.append('tests')
# noinspection PyUnresolvedReferences
import sure
from selenium.webdriver.support.ui import Select
from pages.core.home import HomePage
from pages.events.events_admin import AdminEventPage
from pages.events.home_page_events import EventsHome
from selenium.webdriver import ActionChains
from utils.selenium_helpers import click
from selenium.common.exceptions import TimeoutException
from mimesis import Text

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C68588 - Create daily event with exclusion")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/68588", "TestRail")
class TestC68588:
    def test_c68588(self):

        self.base_url = "http://chipublib.local.bibliocommons.com/events/"

        # log in
        home_page = HomePage(self.driver, self.base_url).open()
        home_page_events = EventsHome(self.driver)
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)

        # navigate to events page
        events_admin = AdminEventPage(self.driver)
        events_admin.admin_link.click()
        events_admin.wait.until(lambda s: events_admin.is_previous_page_icon_displayed)

        # Create events
        events_admin.create_event_button.click()
        events_admin.wait.until(lambda s: events_admin.is_title_textbox_displayed)

        title_name = "test_C68588"

        events_admin.title_textbox.send_keys(title_name)
        try:
            click(events_admin.type_dropdown[0])
        except TimeoutException:
            click(events_admin.type_dropdown[0])
        events_admin.wait.until(lambda s: events_admin.is_type_dropdown_business_displayed)
        events_admin.type_dropdown_business.click()
        events_admin.audience_dropdown.click()
        events_admin.wait.until(lambda s: events_admin.is_type_audience_dropdown_families_displayed)
        events_admin.type_audience_dropdown_families.click()
        description = Text('en').text(quantity=3)
        events_admin.description_field.send_keys(description)
        events_admin.time_selector.click()
        events_admin.wait.until(lambda s: events_admin.start_time[36].is_displayed())
        events_admin.start_time[36].click()
        events_admin.wait.until(lambda s: events_admin.is_event_creation_page_this_event_happens_dropdown_displayed)
        events_admin.event_creation_page_this_event_happens_dropdown.click()

        # location
        events_admin.wait.until(lambda s: events_admin.is_location_dropdown_displayed)
        events_admin.location_dropdown.click()
        events_admin.wait.until(lambda s: events_admin.is_location_dropdown_archer_height_displayed)
        events_admin.location_dropdown_archer.click()

        # this event happens daily selection
        events_admin.wait.until(lambda s: events_admin.event_creation_page_this_event_happens_dropdown)
        event_recurrences = Select(events_admin.event_creation_page_this_event_happens_dropdown)
        event_recurrences.select_by_value("daily")

        events_admin.wait.until(lambda s: events_admin.is_event_start_date_button_displayed)
        events_admin.start_date_button.click()
        events_admin.wait.until(lambda s: events_admin.calendar_next_month_button[0].is_displayed())
        events_admin.calendar_next_month_button[0].click()
        events_admin.wait.until(lambda s: events_admin.calendar_start_end_date_select[13].is_displayed())
        events_admin.calendar_start_end_date_select[13].click()
        events_admin.wait.until(lambda s: events_admin.is_end_date_button_displayed)
        events_admin.end_date_button.click()
        events_admin.wait.until(lambda s: events_admin.calendar_start_end_date_select[46].is_displayed())
        events_admin.calendar_start_end_date_select[46].click()

        # exclusions
        events_admin.exclusion_buttons.click()
        events_admin.wait.until(lambda s: events_admin.exclusion_buttons_find[0].is_displayed())
        events_admin.exclusion_buttons_find[0].click()
        events_admin.wait.until(lambda s: events_admin.exclusion_buttons_find[-1].is_displayed())
        events_admin.exclusion_buttons_find[-1].click()
        events_admin.wait.until(lambda s: events_admin.is_exclusion_done_button_displayed)
        events_admin.exclusion_done_button.click()
        exclusion_date_1 = events_admin.excluding_date_list[0].text
        exclusion_date_2 = events_admin.excluding_date_list[1].text

        events_admin.wait.until(lambda s: events_admin.save_and_publish.is_enabled())
        click(events_admin.save_and_publish)
        events_admin.wait.until(lambda s: events_admin.is_left_chevron_button_displayed)

        # search page for event and verify it appears
        home_page.open()
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)
        if home_page.header.is_collapsible_search_trigger_displayed:
            home_page.header.collapsible_search_trigger.click()
        home_page_events.search_textbox.send_keys(title_name)
        home_page_events.search_textbox_icon.click()

        home_page_events.wait.until(lambda s: home_page_events.is_clear_filters_search_result_link_displayed)
        amount_of_date_headers = len(home_page_events.date_header)

        x = 0
        while x != amount_of_date_headers:
            home_page_events.date_header[x].text.should_not.contain(exclusion_date_1)
            home_page_events.date_header[x].text.should_not.contain(exclusion_date_2)
            x += 1

        home_page.open()
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)
        events_admin.admin_link.click()
        events_admin.wait.until(lambda s: events_admin.is_admin_published_tab_displayed)
        events_admin.admin_published_tab.click()
        events_admin.wait.until(lambda s: events_admin.is_admin_events_search_textbox_displayed)
        events_admin.retry_search_until_results_appear(title_name)

        admin_event_title = events_admin.admin_event_title
        self.driver.execute_script('arguments[0].scrollIntoView(true);', admin_event_title)

        delete_button = events_admin.admin_delete_button
        ActionChains(self.driver).move_to_element(delete_button).perform()
        events_admin.wait.until(lambda s: events_admin.admin_event_action_links[6].is_displayed())
        events_admin.admin_event_action_links[6].click()
        events_admin.delete_this_and_subsequent.click()

        events_admin.wait.until(lambda s: events_admin.is_confirm_delete_button_displayed)
        events_admin.confirm_delete_button.click()
        events_admin.wait.until(lambda s: events_admin.is_close_delete_overlay_button_displayed)
        events_admin.close_delete_overlay_button.click()
        events_admin.wait.until(lambda s: events_admin.assert_is_no_results_displayed)
        events_admin.assert_is_no_results_displayed.should.be.true
