import pytest
import allure
import sure
import configuration.user
import configuration.system
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.create_new_featured_block.create_new_banner import CreateNewBannerPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.wpadmin.v3.all_contents_page.all_banners import AllBannersPage
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from mimesis import Text

BANNER_NAME = ' '.join(Text('en').words(quantity=3))
DESKTOP_TITLE = ' '.join(Text('en').words())
MOBILE_TITLE = ' '.join(Text('en').words())


@pytest.mark.email
@pytest.mark.release
@pytest.mark.stage
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C81876: Banner Module - Successful Banner Creation")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/81876", "TestRail")
class TestC81876:
    def test_C81876(self):
        # Logged in as Network Admin
        login_page = LoginPage(self.driver, configuration.system.base_url_email).open()
        login_page.log_in(configuration.user.user['email']['stage']['admin']['name'],
                          configuration.user.user['email']['stage']['admin']['password'])

        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_email, page='bibliocommons-settings', tab='system').open()
        settings_system_settings_tab.v3_status_enabled.click()
        settings_system_settings_tab.save_changes.click()

        self.driver.delete_all_cookies()

        login_page = LoginPage(self.driver, configuration.system.base_url_email).open()
        login_page.log_in(configuration.user.user['email']['stage']['libadmin']['name'],
                          configuration.user.user['email']['stage']['libadmin']['password'])

        # Create an Email CTA Banner
        create_new_banner = CreateNewBannerPage(self.driver, configuration.system.base_url_email, post_type='bw_banner').open()
        create_new_banner.regular_banner_type.is_displayed().should.be.true
        create_new_banner.email_sign_up_cta.is_displayed().should.be.true
        create_new_banner.email_sign_up_cta.click()
        create_new_banner.name_of_banner.is_displayed().should.be.true
        create_new_banner.name_of_banner.send_keys(BANNER_NAME)
        create_new_banner.desktop_title.is_displayed().should.be.true
        create_new_banner.desktop_title.send_keys(DESKTOP_TITLE)
        create_new_banner.mobile_title.is_displayed().should.be.true
        create_new_banner.mobile_title.send_keys(MOBILE_TITLE)
        create_new_banner.scroll_to_top()
        create_new_banner.publish.click()

        # Assert that the Email CTA Banner was created
        all_banners_page = AllBannersPage(self.driver, configuration.system.base_url_email, page='bw-banner').open()
        all_banners_page.search_input.send_keys(BANNER_NAME)
        all_banners_page.search_button.click()
        wait = WebDriverWait(self.driver, 10, poll_frequency=2, ignored_exceptions=NoSuchElementException)
        wait.until(lambda condition: all_banners_page.rows[0].title.text == BANNER_NAME)

        # Delete the created Banner
        all_banners_page.rows[0].hover_on_title()
        all_banners_page.rows[0].delete.click()
        all_banners_page.rows.should.be.empty
