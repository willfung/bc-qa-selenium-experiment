import pytest
import allure
import sure
import configuration.user
import configuration.system
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.create_new_featured_block.create_new_banner import CreateNewBannerPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage


@pytest.mark.email
@pytest.mark.release
@pytest.mark.stage
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C81707: Banner Module Configuration")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/81707", "TestRail")
class TestC81707:
    def test_C81707(self):
        # Logged in as Network Admin
        login_page = LoginPage(self.driver, configuration.system.base_url_email).open()
        login_page.log_in(configuration.user.user['email']['stage']['admin']['name'],
                          configuration.user.user['email']['stage']['admin']['password'])

        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_email, page='bibliocommons-settings', tab='system').open()
        settings_system_settings_tab.v3_status_enabled.click()
        settings_system_settings_tab.save_changes.click()

        self.driver.delete_all_cookies()

        login_page = LoginPage(self.driver, configuration.system.base_url_email).open()
        login_page.log_in(configuration.user.user['email']['stage']['libadmin']['name'],
                          configuration.user.user['email']['stage']['libadmin']['password'])

        create_new_banner = CreateNewBannerPage(self.driver, configuration.system.base_url_email, post_type='bw_banner').open()
        create_new_banner.regular_banner_type.is_displayed().should.be.true
        create_new_banner.email_sign_up_cta.is_displayed().should.be.true
        create_new_banner.email_sign_up_cta.click()

        create_new_banner.name_of_banner.is_displayed().should.be.true
        create_new_banner.desktop_image.is_displayed().should.be.true
        create_new_banner.desktop_title.is_displayed().should.be.true
        create_new_banner.driver.switch_to.frame(create_new_banner.desktop_description_iframe)
        create_new_banner.desktop_description_body.is_displayed().should.be.true
        create_new_banner.driver.switch_to.default_content()
        create_new_banner.is_desktop_callout_text_displayed.should.be.false
        create_new_banner.is_desktop_callout_url_displayed.should.be.false

        create_new_banner.mobile_title.is_displayed().should.be.true
        create_new_banner.driver.switch_to.frame(create_new_banner.mobile_description_iframe)
        create_new_banner.mobile_description_body.is_displayed().should.be.true
        create_new_banner.driver.switch_to.default_content()
        create_new_banner.is_mobile_callout_text_displayed.should.be.false
        create_new_banner.is_mobile_callout_url_displayed.should.be.false

        create_new_banner.background_colour.is_displayed().should.be.true
        create_new_banner.image_position.is_displayed().should.be.true
        create_new_banner.title_colour.is_displayed().should.be.true
        create_new_banner.description_colour.is_displayed().should.be.true

        create_new_banner.taxonomy("audience").is_displayed().should.be.true
        create_new_banner.taxonomy("related format").is_displayed().should.be.true
        create_new_banner.taxonomy("programs and campaigns").is_displayed().should.be.true
        create_new_banner.taxonomy("topic").is_displayed().should.be.true
        create_new_banner.taxonomy("tags").is_displayed().should.be.true
