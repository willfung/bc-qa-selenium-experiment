import os
import sys
sys.path.append('tests')
import FuelSDK

try:
    debug = False
    stubObj = FuelSDK.ET_Client(False, debug,
        {
            'clientid': os.environ.get("CLIENT_ID"),
            'clientsecret': os.environ.get("CLIENT_PASSWORD"),
            'defaultwsdl': os.environ.get("DEFAULT_WSDL"),
            'soapendpoint': os.environ.get("SOAP_ENDPOINT"),
            'wsdl_file_local_loc': os.environ.get("WSDL_FILE_LOCAL_LOC")
        })

    # Get all of the DataExtensions in an Account
    print('>>> Get all of the DataExtensions in an Account')
    de = FuelSDK.ET_DataExtension()
    de.auth_stub = stubObj
    de.props = ["CustomerKey", "Name"]
    get_response = de.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('RequestID: ' + str(get_response.request_id))
    print('Results Length: ' + str(len(get_response.results)))
    # print 'Results: ' + str(get_response.results)

    # Get all of the DataExtensions in an Account belonging to a specific sub account
    print('>>> Get all of the DataExtensions in an Account belonging to a specific sub account')
    de = FuelSDK.ET_DataExtension()
    de.auth_stub = stubObj
    de.props = ["CustomerKey", "Name"]
    de.options = {"Client": {"ID": "1234567"}}
    get_response = de.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('RequestID: ' + str(get_response.request_id))
    print('Results Length: ' + str(len(get_response.results)))
    # print 'Results: ' + str(get_response.results)

    # Specify a name for the data extension that will be used for testing
    # Note: Name and CustomerKey will be the same value
    # WARNING: Data Extension will be deleted so don't use the name of a
    # production data extension
    name_of_DE = "ThisWillBeDeleted-Test"

    # Create  Data Extension
    print('>>> Create Data Extension')
    de2 = FuelSDK.ET_DataExtension()
    de2.auth_stub = stubObj
    de2.props = {"Name": name_of_DE, "CustomerKey": name_of_DE}
    de2.columns = [{"Name": "Name", "FieldType": "Text", "IsPrimaryKey": "true", "MaxLength": "100", "IsRequired": "true"},
                   {"Name": "OtherField", "FieldType": "Text"}]
    post_response = de2.post()
    print('Post Status: ' + str(post_response.status))
    print('Code: ' + str(post_response.code))
    print('Message: ' + str(post_response.message))
    print('Results: ' + str(post_response.results))
    # Update DE to add new field
    print('>>> Update DE to add new field')
    de3 = FuelSDK.ET_DataExtension()
    de3.auth_stub = stubObj
    de3.props = {"Name": name_of_DE, "CustomerKey": name_of_DE}
    de3.columns = [{"Name": "AddedField", "FieldType": "Text"}]
    patch_response = de3.patch()
    print('Patch Status: ' + str(patch_response.status))
    print('Code: ' + str(patch_response.code))
    print('Message: ' + str(patch_response.message))
    print('Results: ' + str(patch_response.results))

    # Retrieve all columns for data extension
    print('>>> Retrieve all columns for data extension ')
    my_DE_column = FuelSDK.ET_DataExtension_Column()
    my_DE_column.auth_stub = stubObj
    my_DE_column.props = ["Name"]
    my_DE_column.search_filter = {'Property': 'CustomerKey', 'SimpleOperator': 'equals', 'Value': name_of_DE}
    get_response = my_DE_column.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('RequestID: ' + str(get_response.request_id))
    print('Results Length: ' + str(len(get_response.results)))
    print('Results: ' + str(get_response.results))

    # Add a row to a data extension (using CustomerKey)
    print('>>>  Add a row to a data extension')
    de4 = FuelSDK.ET_DataExtension_Row()
    de4.CustomerKey = name_of_DE
    de4.auth_stub = stubObj
    de4.props = {"Name": "MAC3", "OtherField": "Text3"}
    post_response = de4.post()
    print('Post Status: ' + str(post_response.status))
    print('Code: ' + str(post_response.code))
    print('Message: ' + str(post_response.message))
    print('Results: ' + str(post_response.results))

    # Add a row to a data extension (Using Name)
    print('>>> Add a row to a data extension')
    de4 = FuelSDK.ET_DataExtension_Row()
    de4.auth_stub = stubObj
    de4.Name = name_of_DE
    de4.props = {"Name": "MAC4", "OtherField": "Text3"}
    post_response = de4.post()
    print('Post Status: ' + str(post_response.status))
    print('Code: ' + str(post_response.code))
    print('Message: ' + str(post_response.message))
    print('Results: ' + str(post_response.results))

    # Retrieve all rows
    print('>>> Retrieve all rows')
    row = FuelSDK.ET_DataExtension_Row()
    row.auth_stub = stubObj
    row.CustomerKey = name_of_DE
    row.props = ["Name", "OtherField"]
    get_response = row.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('RequestID: ' + str(get_response.request_id))
    print('Results Length: ' + str(len(get_response.results)))
    print('Results: ' + str(get_response.results))

    # Update a row in  a data extension
    print('>>> Update a row in  a data extension')
    de4 = FuelSDK.ET_DataExtension_Row()
    de4.auth_stub = stubObj
    de4.CustomerKey = name_of_DE
    de4.props = {"Name": "MAC3", "OtherField": "UPDATED!"}
    post_response = de4.patch()
    print('Patch Status: ' + str(post_response.status))
    print('Code: ' + str(post_response.code))
    print('Message: ' + str(post_response.message))
    print('Results: ' + str(post_response.results))

    # Retrieve only updated row
    print('>>> Retrieve only updated row')
    row = FuelSDK.ET_DataExtension_Row()
    row.auth_stub = stubObj
    row.CustomerKey = name_of_DE
    row.props = ["Name", "OtherField"]
    row.search_filter = {'Property': 'Name', 'SimpleOperator': 'equals', 'Value': 'MAC3'}
    get_response = row.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('RequestID: ' + str(get_response.request_id))
    print('Results Length: ' + str(len(get_response.results)))
    print('Results: ' + str(get_response.results))

    # Delete a row from a data extension
    print('>>> Delete a row from a data extension')
    de4 = FuelSDK.ET_DataExtension_Row()
    de4.auth_stub = stubObj
    de4.CustomerKey = name_of_DE
    de4.props = {"Name": "MAC3"}
    deleteResponse = de4.delete()
    print('Delete Status: ' + str(deleteResponse.status))
    print('Code: ' + str(deleteResponse.code))
    print('Message: ' + str(deleteResponse.message))
    print('Results: ' + str(deleteResponse.results))

    # Delete a Data Extension
    print('>>> Delete a  Data Extension')
    de5 = FuelSDK.ET_DataExtension()
    de5.auth_stub = stubObj
    de5.props = {"Name": name_of_DE, "CustomerKey": name_of_DE}
    delResponse = de5.delete()
    print('Delete Status: ' + str(delResponse.status))
    print('Code: ' + str(delResponse.code))
    print('Message: ' + str(delResponse.message))
    print('Results: ' + str(delResponse.results))

    # Retrieve lots of rows with moreResults
    print('>>> Retrieve lots of rows with moreResults')
    row = FuelSDK.ET_DataExtension_Row()
    row.auth_stub = stubObj
    row.Name = "zipstolong"
    row.props = ["zip", "latitude", "longitude"]
    get_response = row.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('RequestID: ' + str(get_response.request_id))
    print('Results Length: ' + str(len(get_response.results)))
    # print 'Results: ' + str(get_response.results)

    while get_response.more_results:
        print('>>> Continue Retrieve lots of rows with moreResults')
        get_response = row.getMoreResults()
        print('Retrieve Status: ' + str(get_response.status))
        print('Code: ' + str(get_response.code))
        print('Message: ' + str(get_response.message))
        print('MoreResults: ' + str(get_response.more_results))
        print('RequestID: ' + str(get_response.request_id))
        print('Results Length: ' + str(len(get_response.results)))

except Exception as e:
    print('Caught exception: ' + str(e))
