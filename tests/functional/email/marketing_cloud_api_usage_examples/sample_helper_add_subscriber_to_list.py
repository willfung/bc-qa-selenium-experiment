import os
import sys
sys.path.append('tests')
import FuelSDK

try:
    debug = False
    stubObj = FuelSDK.ET_Client(False, debug,
        {
            'clientid': os.environ.get("CLIENT_ID"),
            'clientsecret': os.environ.get("CLIENT_PASSWORD"),
            'defaultwsdl': os.environ.get("DEFAULT_WSDL"),
            'soapendpoint': os.environ.get("SOAP_ENDPOINT"),
            'wsdl_file_local_loc': os.environ.get("WSDL_FILE_LOCAL_LOC")
        })
    
    NewListName = "PythonSDKList"

    # Example using AddSubscriberToList() method
    # Typically this method will be used with a pre-existing list but for testing purposes one is being created.
    
    # Create List 
    print('>>> Create List')
    post_list = FuelSDK.ET_List()
    post_list.auth_stub = stubObj
    post_list.props = {"ListName": NewListName, "Description": "This list was created with the PythonSDK", "Type": "Private"}
    post_response = post_list.post()
    print('Post Status: ' + str(post_response.status))
    print('Code: ' + str(post_response.code))
    print('Message: ' + str(post_response.message))
    print('Result Count: ' + str(len(post_response.results)))
    print('Results: ' + str(post_response.results))
    
    if post_response.status:
        
        new_list_id = post_response.results[0]['NewID']
        # Adding Subscriber To a List
        print('>>> Add Subscriber To a List')
        add_sub_response = stubObj.AddSubscriberToList("AddSubTesting@bh.exacttarget.com", [new_list_id])
        print('add_sub_response Status: ' + str(add_sub_response.status))
        print('Code: ' + str(add_sub_response.code))
        print('Message: ' + str(add_sub_response.message))
        print('Result Count: ' + str(len(add_sub_response.results)))
        print('Results: ' + str(add_sub_response.results))
                
        # Delete List
        print('>>> Delete List')
        delete_sub = FuelSDK.ET_List()
        delete_sub.auth_stub = stubObj
        delete_sub.props = {"ID": new_list_id}
        delete_response = delete_sub.delete()
        print('Delete Status: ' + str(delete_response.status))
        print('Code: ' + str(delete_response.code))
        print('Message: ' + str(delete_response.message))
        print('Results Length: ' + str(len(delete_response.results)))
        print('Results: ' + str(delete_response.results))

except Exception as e:
    print('Caught exception: ' + str(e))
