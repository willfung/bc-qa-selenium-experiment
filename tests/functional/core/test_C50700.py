import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.home import HomePage
from pages.core.bib import BibPage
from pages.core.v2.holds import HoldsPage

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C50700: Cancel hold from bib page")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C50700", "TestRail")
class TestC50700:
    def test_C50700(self):
        self.item_id = "2000853126" #Title: "Waking up in Paris"
        self.name = "D059874977"
        self.password = "60605"
        self.base_url = "https://chipublib.demo.bibliocommons.com"

        bib_page = BibPage(self.driver, self.base_url, item_id=self.item_id).open()
        bib_page.header.log_in(self.name, self.password)
        bib_page.wait.until(lambda s: bib_page.circulation.is_place_hold_button_displayed)
        bib_page.circulation.place_hold.click()
        bib_page.circulation.confirm_hold.click()

        holds_page = HoldsPage(self.driver, self.base_url).open()
        holds_page.wait.until(lambda s: holds_page.holds_list_is_displayed)
        holds_page.items[0].bib_title.text.should.contain("Waking up in Paris")

        bib_page = BibPage(self.driver, self.base_url, item_id=self.item_id).open()
        bib_page.wait.until(lambda s: bib_page.circulation.is_cancel_hold_displayed)
        bib_page.circulation.cancel_hold.click()
        bib_page.wait.until(lambda s: bib_page.overlay.bib_cancel.is_confirm_cancel_hold_displayed)
        bib_page.overlay.bib_cancel.confirm_cancel_hold.click()

        bib_page.wait.until(lambda s: bib_page.circulation.is_place_hold_button_displayed)
        bib_page.circulation.is_place_hold_button_displayed.should.be.true
        bib_page.is_top_message_displayed.should.be.true
