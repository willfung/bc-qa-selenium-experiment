import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.bib import BibPage
from pages.core.all_comments import BibCommentPage

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C47054: Click View All Comments on bib page")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C47054", "TestRail")
class TestC47054:
    def test_C47054(self):
        base_url = "https://chipublib.demo.bibliocommons.com/"

        bib_page = BibPage(self.driver, base_url, item_id=1904532126).open()
        bib_page.community_activity.comment.view_all_comments.click()
        bib_all_comments_page = BibCommentPage(self.driver)
        bib_all_comments_page.wait.until(lambda s: (len(bib_all_comments_page.comment_items) > 24))
        len(bib_all_comments_page.comment_items).should.equal(25)
