import pytest
import allure
import sure
import configuration.user
import configuration.system
from selenium.common.exceptions import NoSuchElementException, ElementNotInteractableException
from selenium.webdriver.support.ui import WebDriverWait
from mimesis import Text
from utils.image_download_helper import *
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.wpadmin.v3.create_new_content.create_new_blog_post import CreateNewBlogPostPage
from pages.web.page_builder import PageBuilderPage
from pages.web.user import UserPage
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.wpadmin.v3.all_contents_page.all_blog_posts import AllBlogPostsPage
from pages.web.wpadmin.v3.edit_blog_post import EditBlogPostPage
from pages.web.blog_post import BlogPostPage
from utils.db_connector import DBConnector
from utils.selenium_helpers import click
from pages.web.wpadmin.v3.default_settings_page_builder_page import DefaultSettingsPageBuilderPage
from pages.web.wpadmin.v3.settings_general_tab import SettingsGeneralTabPage
from pages.web.wpadmin.v3.media_library.media_library_list import MediaLibraryListPage
from pages.web.wpadmin.v3.all_contents_page.all_cards import AllCardsPage


PAGE = "bibliocommons-settings"
BLOG_INFO = {
    'title': '-'.join(Text('en').words(quantity=3)),
    'description': Text('en').text(quantity=1)
}
IMAGE_TITLE = Text('en').words(quantity=1)[0]
IMAGE_PATH = get_image_path_name(IMAGE_TITLE, ".jpg")
UPDATED_BLOG_TITLE = "UPDATED: " + BLOG_INFO['title']
UPDATED_BLOG_DESCRIPTION = "UPDATED: " + BLOG_INFO['description']
TAXONOMIES = ['Audience', 'Related Format', 'Programs and Campaigns', 'Genre', 'Topic', 'Tags']
taxonomies_terms = []
PAGE_TITLE = '-'.join(Text('en').words(quantity=3))
CONTENT_TYPE = "Blog Post"


@pytest.fixture(scope='class')
def login_and_setup(request, selenium_setup_and_teardown):

    driver = request.cls.driver

    # Log in as Network Admin
    login_page = LoginPage(driver, configuration.system.base_url_web).open()
    login_page.log_in(configuration.user.user['web']['local']['admin']['name'],
                      configuration.user.user['web']['local']['admin']['password'])
    settings_system_settings_tab = SettingsSystemSettingsTabPage(driver, configuration.system.base_url_web,
                                                                 page=PAGE, tab='system').open()
    settings_system_settings_tab.v3_status_enabled.click()
    settings_system_settings_tab.save_changes.click()

    settings_general_tab = SettingsGeneralTabPage(driver, configuration.system.base_url_web, page=PAGE,
                                                  tab='generic').open()
    click(settings_general_tab.enable_make_tags_a_structured_taxonomy)
    settings_general_tab.save_changes.click()

    default_settings_page = DefaultSettingsPageBuilderPage(driver, configuration.system.base_url_web,
                                                           post_type='fl-builder-template',
                                                           page='default-page-builder-settings').open()
    default_settings_page.check_all_display_taxonomy_links_checkboxes()
    default_settings_page.save_changes_button_click()

    new_blog_post = CreateNewBlogPostPage(driver, configuration.system.base_url_web, post_type='post').open()

    # Creating a dynamic list of taxonomy terms, so that the test can be run on any environment
    for index, taxonomy in enumerate(TAXONOMIES):
        click(new_blog_post.taxonomy(taxonomy))
        taxonomies_terms.append(new_blog_post.taxonomy_terms(taxonomy)[0])
        if index == len(TAXONOMIES) - 1:
            break
        new_blog_post.taxonomies_evergreen_checkbox.click()

    # Logging out
    driver.delete_all_cookies()


@pytest.mark.usefixtures('login_and_setup')
@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
class Tests:
    @allure.title("C87830: Blog Post - Create New")
    @allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87830", "TestRail")
    def test_C87830(self):

        # Logging in as lib admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        download_image("https://bit.ly/39sMQVm", IMAGE_PATH)

        # PART 1- Create new Blog Post/Card
        new_blog_post = CreateNewBlogPostPage(self.driver, configuration.system.base_url_web, post_type='post').open()
        new_blog_post.page_heading.text.should.match("Create New Blog Post")
        new_blog_post.title.send_keys(BLOG_INFO['title'])

        new_blog_post.add_media.click()
        new_blog_post.insert_media.upload_image.send_keys(IMAGE_PATH)
        new_blog_post.insert_media.add_image_to_widget_button.click()
        self.driver.switch_to.frame(new_blog_post.visual_area.iframe)
        new_blog_post.visual_area.body.send_keys(BLOG_INFO['description'])
        self.driver.switch_to.default_content()

        # click(new_blog_post.visual)
        visual = new_blog_post.visual_area

        # Adding a FAQ widget
        visual.related_faq_button()
        visual.related_faq.select_category("Most Recent")
        visual.related_faq.insert_shortcode.click()
        visual.is_related_faq_form_visible.should.be.false

        # Adding a location widget
        visual.add_library()
        visual.add_library_locations.choose_library_locations("Albany Park").click()
        visual.add_library_locations.choose_library_locations("Avalon").click()
        visual.add_library_locations.insert_into_post.click()
        visual.is_add_library_locations_form_visible.should.be.false

        click(new_blog_post.autofill_text_fields)
        click(new_blog_post.card_image)
        new_blog_post.select_widget_image.select_image_from_list(0).click()
        click(new_blog_post.select_widget_image.add_image_to_widget_button)
        new_blog_post.select_default_image_crop_views()

        for i in range(len(TAXONOMIES)):
            new_blog_post.select_taxonomy_term(TAXONOMIES[i], taxonomies_terms[i])

        new_blog_post.taxonomies_featured_checkbox.click()
        new_blog_post.taxonomies_evergreen_checkbox.click()

        new_blog_post.scroll_to_top()
        click(new_blog_post.publish)

        # Opening a new tab
        self.driver.execute_script("window.open();")
        self.driver.switch_to_window(self.driver.window_handles[1])

        # PART 2 - Create a New Page
        create_new_page = CreateNewPagePage(self.driver, configuration.system.base_url_web, post_type='page').open()
        create_new_page.title.send_keys(PAGE_TITLE)
        create_new_page.page_builder_section.click()
        click(create_new_page.publish)
        create_new_page.wait.until(lambda condition: self.driver.page_source.should.contain("Update"))

        # Opening the new page in the frontend
        new_page = PageBuilderPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        new_page.wait.until(lambda s: new_page.wpheader.is_wp_admin_header_displayed)
        new_page.wpheader.page_builder.click()

        # Select single card module
        new_page.add_single_card(CONTENT_TYPE, BLOG_INFO['title'])
        wait = WebDriverWait(self.driver, 16, poll_frequency=2, ignored_exceptions=[IndexError, NoSuchElementException])
        wait.until(lambda s: new_page.user_facing_modules.single_cards[0].cards[0].is_card_title_displayed)
        new_page.page_builder.done_and_publish()
        new_page.log_out_from_header()

        delete_downloaded_image(IMAGE_PATH)

        self.driver.close()

        # Switching to tab 1
        self.driver.switch_to_window(self.driver.window_handles[0])

    @allure.title("C87831: Blog Post/Card - View")
    @allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87831", "TestRail")
    def test_C87831(self):

        # Logging out
        self.driver.delete_all_cookies()

        # Navigate to blog post
        BlogPostPage(self.driver, configuration.system.base_url_web, title=BLOG_INFO['title']).open()
        _ = [BLOG_INFO['title'], "Albany Park", "Avalon", "Related FAQs", "data-wp-image"]
        for index, information in enumerate(_):
            self.driver.page_source.should.contain(information)

        page_patron = UserPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        page_patron.wpheader.is_wp_admin_header_displayed.should.be.false
        len(page_patron.user_facing_modules.single_cards).should.equal(1)
        page_patron.user_facing_modules.single_cards[0].cards[0].card_title.text.should.match(BLOG_INFO['title'])
        page_patron.user_facing_modules.single_cards[0].cards[0].is_card_content_type_displayed.should.be.true
        len(page_patron.user_facing_modules.single_cards[0].cards[0].card_tags).should.equal(5)

        for i in range(5):
            page_patron.user_facing_modules.single_cards[0].cards[0].card_tags[i].text.should.match(taxonomies_terms[i])

        # Navigate to the page containing the blog crd
        UserPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()

        # Asserting that the required information from the blog post card is present
        _ = [BLOG_INFO['title'], "Albany Park", "Avalon", "o-card__image"]
        for index, information in enumerate(_):
            self.driver.page_source.should.contain(information)

    @allure.title("C87832: Blog Post - Update- Blog card (with taxonomies) already created and added to a PB page")
    @allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87832", "TestRail")
    def test_C87832(self):

        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        # Getting the blog post ID
        all_blogs_page = AllBlogPostsPage(self.driver, configuration.system.base_url_web, post_type='post').open()
        all_blogs_page.search_content_by_button(BLOG_INFO['title'])
        all_blogs_page.rows[0].title.text.should.match(BLOG_INFO['title'])
        _ = all_blogs_page.rows[0].title.get_attribute("href")
        blog_post_id = _[58:-12]

        # Checking that the created blog post contains all the expected information
        edit_blog_post = EditBlogPostPage(self.driver, configuration.system.base_url_web, post=blog_post_id, action='edit').open()
        edit_blog_post.page_heading.text.should.match("Edit Blog Post")
        edit_blog_post.image_cropper.is_crop_one_preview_visible.should.be.true
        edit_blog_post.image_cropper.is_crop_two_preview_visible.should.be.true

        for index, term in enumerate(taxonomies_terms):
            edit_blog_post.selected_taxonomy_terms[index].text.casefold().should.equal(term.casefold())

        # Editing the blog post
        edit_blog_post.title.clear()
        edit_blog_post.title.send_keys(UPDATED_BLOG_TITLE)

        self.driver.switch_to.frame(edit_blog_post.visual_area.iframe)
        edit_blog_post.visual_area.body.clear()
        edit_blog_post.visual_area.body.send_keys(UPDATED_BLOG_DESCRIPTION)
        self.driver.switch_to.default_content()

        # Firefox doesn't need this block of code, but for some reason Chrome fails without it.
        if self.driver.capabilities['browserName'] == 'chrome':
            edit_blog_post.scroll_to_card_image()

        click(edit_blog_post.autofill_text_fields)

        # The following occurs sometimes.
        # The driver sometimes clicks the image_cropper element, which opens up the modal.
        # Trying to mitigate that with the below block of code.
        try:
            if edit_blog_post.image_cropper.cancel.is_displayed():
                edit_blog_post.image_cropper.cancel.click()
        except NoSuchElementException:
            pass

        edit_blog_post.scroll_to_top()

        # Sometimes, the image from the bottom of the page moves to the top upon scrolling.
        # The below block of code is to get past that.
        try:
            click(edit_blog_post.update)
        except ElementNotInteractableException:
            self.driver.execute_script("arguments[0].click();", edit_blog_post.update)

        self.driver.execute_script("window.open();")
        self.driver.switch_to_window(self.driver.window_handles[1])

        # Opening the blog post while still logged in as Staff
        blog_post_page = BlogPostPage(self.driver, configuration.system.base_url_web, title=BLOG_INFO['title']).open()
        self.driver.refresh()
        blog_post_page.blog_title.text.should.match(UPDATED_BLOG_TITLE)

        page = UserPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        self.driver.refresh()
        page.user_facing_modules.single_cards[0].cards[0].card_title.text.should.match(UPDATED_BLOG_TITLE)
        page.user_facing_modules.single_cards[0].cards[0].card_description.text.should.match(UPDATED_BLOG_DESCRIPTION)

        self.driver.close()
        self.driver.switch_to_window(self.driver.window_handles[0])

        # Logging out
        self.driver.delete_all_cookies()

    @allure.title("C87833: Blog Post/Card - Update - View")
    @allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87833", "TestRail")
    def test_C87833(self):

        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        # Getting the blog post ID
        all_blogs_page = AllBlogPostsPage(self.driver, configuration.system.base_url_web, post_type='post').open()
        all_blogs_page.search_content_by_button(UPDATED_BLOG_TITLE)
        all_blogs_page.rows[0].title.text.should.match(UPDATED_BLOG_TITLE)
        _ = all_blogs_page.rows[0].title.get_attribute("href")
        blog_post_id = _[58:-12]

        # Checking the DB if the blog's title and description are updated
        check_db = DBConnector()
        check_db.check_updated_post_title(UPDATED_BLOG_TITLE, blog_post_id).should.be.true
        check_db.check_updated_post_description(UPDATED_BLOG_DESCRIPTION, blog_post_id).should.be.true

        # Deleting the created blog post
        all_blogs_page = AllBlogPostsPage(self.driver, configuration.system.base_url_web, post_type='post').open()
        all_blogs_page.search_and_delete(UPDATED_BLOG_TITLE)
        all_blogs_page.rows.should.be.empty

        all_cards_page = AllCardsPage(self.driver, configuration.system.base_url_web, page='bw-content-card').open()
        all_cards_page.search_content_by_button(UPDATED_BLOG_TITLE)
        all_cards_page.rows.should.be.empty

        # Deleting the created page
        all_pages_page = AllPagesPage(self.driver, configuration.system.base_url_web, post_type='page').open()
        all_pages_page.search_and_delete(PAGE_TITLE)
        all_pages_page.rows.should.be.empty

        # Deleting the uploaded image from the media library
        media_library_page = MediaLibraryListPage(self.driver, configuration.system.base_url_web, mode='list').open()
        media_library_page.search_and_delete(IMAGE_TITLE)
        media_library_page.rows.should.be.empty
