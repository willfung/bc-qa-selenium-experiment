import pytest
import allure
import sure
import configuration.user
import configuration.system
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.wpadmin.v3.create_a_new_card.create_new_twitter_card import CreateNewTwitterCard
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.page_builder import PageBuilderPage
from pages.web.user import UserPage
from pages.web.wpadmin.v3.all_contents_page.all_cards import AllCardsPage
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage
from mimesis import Text
from utils.selenium_helpers import click


PAGE = 'bibliocommons-settings'
TWITTER_INFO = {
    'username': "680NEWStraffic",
    'screename': "680 NEWS Traffic",
    'tweet_id': "1212825122731708416"
}
PAGE_TITLE = '-'.join(Text('en').words(quantity=3))
CONTENT_TYPE = "Twitter"


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C87845: Create new card - Twitter Card")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87845", "TestRail")
class TestC87845:
    def test_C87845(self):

        # Log in as Network Admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'],
                          configuration.user.user['web']['local']['admin']['password'])
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web,
                                                                     page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_enabled.click()
        settings_system_settings_tab.save_changes.click()

        self.driver.delete_all_cookies()

        # Creating new Twitter Card
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        new_twitter_card = CreateNewTwitterCard(self.driver, configuration.system.base_url_web, post_type='bw_twitter').open()
        new_twitter_card.page_heading.text.should.match("Create New Twitter Card")
        twitter_url = "https://twitter.com/" + TWITTER_INFO['username'] + "/status/" + TWITTER_INFO['tweet_id']
        new_twitter_card.tweet_url.send_keys(twitter_url)
        new_twitter_card.grab_tweet_info.click()
        wait = WebDriverWait(self.driver, 16, poll_frequency=2, ignored_exceptions=[IndexError, NoSuchElementException])
        wait.until(lambda condition: new_twitter_card.is_validator_message_displayed)
        new_twitter_card.is_validator_success_message_displayed.should.be.true
        new_twitter_card.validator_success_message.text.should.match("Tweet information successfully grabbed!")
        new_twitter_card.publish.click()
        new_twitter_card.is_success_message_displayed.should.be.true
        new_twitter_card.wpsidemenu.menu_all_content.click()
        new_twitter_card.wpsidemenu.submenu_all_pages.click()

        # Launching PB from existing page
        # PART 3 - Create a New Page
        new_page = CreateNewPagePage(self.driver, configuration.system.base_url_web, post_type='page').open()
        new_page.title.send_keys(PAGE_TITLE)
        new_page.page_builder_section.click()
        click(new_page.publish)

        all_pages_page = AllPagesPage(self.driver, configuration.system.base_url_web, post_type='page').open()
        all_pages_page.launch_page_builder(PAGE_TITLE)

        # Select single card module
        page_builder_page = PageBuilderPage(self.driver)
        page_builder_page.add_single_card(CONTENT_TYPE, TWITTER_INFO['screename'])
        wait.until(lambda s: page_builder_page.user_facing_modules.single_cards[0].cards[0].is_card_title_displayed)
        page_builder_page.page_builder.done_and_publish()
        page_builder_page.log_out_from_header()

        # PART 4 - Patron view
        self.driver.delete_all_cookies()
        page_patron = UserPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        page_patron.wpheader.is_wp_admin_header_displayed.should.be.false
        len(page_patron.user_facing_modules.single_cards).should.equal(1)
        page_patron.user_facing_modules.single_cards[0].cards[0].card_author.text.should.contain(TWITTER_INFO['screename'])
        page_patron.user_facing_modules.single_cards[0].cards[0].is_card_content_type_displayed.should.be.false

        # PART 5 - Delete contents
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        all_pages_page = AllPagesPage(self.driver, configuration.system.base_url_web, post_type='page').open()
        all_pages_page.search_and_delete(PAGE_TITLE)
        all_pages_page.rows.should.be.empty

        all_cards_page = AllCardsPage(self.driver, configuration.system.base_url_web, page='bw-content-card').open()
        all_cards_page.search_and_delete(TWITTER_INFO['screename'])
        all_cards_page.rows.should.be.empty
