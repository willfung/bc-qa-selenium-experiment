import requests
import re
import json
from collections import namedtuple
from pprint import pprint
from tqdm import tqdm


class NoMatchForQueryError(Exception):
    """Raised when the query results in no matches found"""
    pass


class BCAPIGateway:
    def __init__(self, base_url):
        removed_scheme = base_url.replace('https://', '')
        split_url = removed_scheme.split('.', 1)

        env = split_url[1]
        library = split_url[0]
        self.endpoint = 'https://gateway.{}/v2/libraries/{}/'.format(env, library)


class BCAPIGatewayBibs(BCAPIGateway):
    def __init__(self, base_url):
        BCAPIGateway.__init__(self, base_url)

    # Returns a list of available 'filters' for a given library (e.g ['BK', 'EBOOK', 'AB', 'NEWSPAPER']):
    @property
    def filters(self):
        # endpoint = 'https://gateway.{}/v2/libraries/{}/bibs/search?query=ALL&searchType=smart'.format(self.env, self.library)
        response = requests.get(self.endpoint + 'bibs/search?query=ALL&searchType=smart')
        field_filters = response.json()['catalogSearch']['fields'][0]['fieldFilters']
        filters = []
        for field in field_filters:
            filters.append({'value': field['value'], 'groups': str(field['groupIds']), 'count': field['count']})
        return filters

    def search(
        self,
        term,
        search_type = "keyword",
        format = "BK",
        type = "PHYSICAL",
        provider = "ILS",
        available = 0, # 1,
        holds = 0,
        pages = 2
    ):

        # Initialize an empty list to hold query matches:
        items = []

        # endpoint = 'https://gateway.{}/v2/libraries/{}/bibs/search?query={}&searchType={}&f_FORMAT={}'.format(self.env, self.library, term, search_type, format)
        response = requests.get(self.endpoint + 'bibs/search?query={}&searchType={}&f_FORMAT={}'.format(term, search_type, format))
        total_pages = response.json()['catalogSearch']['pagination']['pages']
        if pages <= total_pages:
            pass
        else:
            print('{} page(s) requested but only {} page(s) are available for this query.'.format(pages, total_pages))
            pages = total_pages

        print("\nRunning BC API Gateway query for:\n{}bibs/search?query={}&searchType={}&f_FORMAT={}&page=<n>".format(self.endpoint, term, search_type, format))
        for i in tqdm(range(1, pages + 1), unit="reqs"):
            response = requests.get(self.endpoint + 'bibs/search?query={}&searchType={}&f_FORMAT={}&page={}'.format(term, search_type, format, i))
            body = response.json()
            try:
                bibs = body["entities"]["bibs"]
            except KeyError as exception:
                print('Response Code: {}'.format(response.status_code))
                print('Response Body: {}'.format(body))
                raise exception

            for bib in bibs:
                # Availability = namedtuple("Availability", "total available on_hold")
                Availability = namedtuple("Availability", "total available on_hold library_use_only status")
                Bib = namedtuple("Bib", "availability format type provider title subtitle eresourceURL id api_id authors")
                Bib.__new__.__defaults__ = (None, None, None, None, None, None, None, None, None, None)

                # Populate availability details:
                if bibs[bib]["availability"]["totalCopies"] is not None:
                    total_copies = int(bibs[bib]["availability"]["totalCopies"])
                else:
                    total_copies = 0

                if bibs[bib]["availability"]["availableCopies"] is not None:
                    available_copies = int(bibs[bib]["availability"]["availableCopies"])
                else:
                    available_copies = 0

                if bibs[bib]["availability"]["heldCopies"] is not None:
                    held_copies = int(bibs[bib]["availability"]["heldCopies"])
                else:
                    held_copies = 0
                # availability = Availability(total = total_copies, available = available_copies, on_hold = held_copies)
                availability = Availability(total=total_copies, available=available_copies, on_hold=held_copies,
                                            library_use_only=bibs[bib]["availability"]["libraryUseOnly"],
                                            status=bibs[bib]["availability"]["localisedStatus"]
                                            )
                # Format item ID:
                split_id = re.split("S|C", bib)
                split_id.pop(0)
                # Populate item details:
                item = Bib(
                    availability = availability,
                    format = bibs[bib]["briefInfo"]["format"],
                    type = bibs[bib]["availability"]["bibType"],
                    provider = bibs[bib]["policy"]["provider"],
                    title = bibs[bib]["briefInfo"]["title"],
                    subtitle = bibs[bib]["briefInfo"]["subtitle"],
                    eresourceURL = bibs[bib]["availability"]["eresourceUrl"],
                    id = split_id[1] + split_id[0].zfill(3),
                    api_id = bibs[bib]["id"],
                    authors=bibs[bib]["briefInfo"]["authors"]
                    )
                # if item.availability.available >= available and item.availability.on_hold >= holds:
                #     items.append(item)
                # if available == 0:
                #     if item.availability.available == 0 and \
                #         item.availability.on_hold >= holds and \
                #         item.availability.library_use_only is False \
                #                 and item.provider == provider:
                #         items.append(item)
                # # elif item.availability.available >= available and \
                # #         item.availability.on_hold >= holds and \
                # #         item.availability.library_use_only is False \
                # #         and item.provider == provider:
                # #     items.append(item)
                # else:
                #     if item.availability.available >= available and \
                #         item.availability.on_hold >= holds and \
                #         item.availability.library_use_only is False \
                #             and item.provider == provider:
                #         items.append(item)

                if item.availability.available >= available and \
                    item.availability.on_hold >= holds and \
                    item.availability.library_use_only is False \
                        and item.provider == provider:
                    items.append(item)

        # Raise an error if no matching results are found:
        if len(items) == 0:
            print("No results found for the given query parameters:")
            parameters = {
                "Search Term": term,
                "Search Type": search_type,
                "Item Format": format,
                "Item Provider": provider,
                "Available Copies": available,
                "On Hold Copies": holds
            }
            pprint(parameters)
            print(50 * "-")
            raise NoMatchForQueryError

        return items


class BCAPIGatewaySessions(BCAPIGateway):
    def __init__(self, base_url):
        BCAPIGateway.__init__(self, base_url)

    def create(self, user_name, password):
        payload = json.dumps({"username": user_name, "password": password})
        response = requests.post(self.endpoint + 'sessions', payload, headers={'Content-Type': 'application/json'})

        body = response.json()
        user_id = body['auth']['currentUserId']
        for cookie in body['proxiedCookies']:
            if 'bc_access_token' in cookie:
                cookies = cookie.split(';')
                break
        access_token = cookies[0]

        return user_id, access_token


class BCAPIGatewayShelves(BCAPIGateway):
    def __init__(self, base_url):
        BCAPIGateway.__init__(self, base_url)

    def add_items_to_shelf(self, user_id, token, items, shelf):
        cookie = {token.split('=')[0]: token.split('=')[1]}

        payload = json.dumps({"userId": user_id, "metadataIds": items, "shelfName": shelf})
        response = requests.post(self.endpoint + 'shelves',
                                 payload,
                                 headers={'Content-Type': 'application/json'},
                                 cookies=cookie
                                 )

        if response.status_code is not 200:
            print(response.status_code)
            print(response.json())

            print(self.endpoint + 'shelves')
            print(payload)
            print(cookie)
