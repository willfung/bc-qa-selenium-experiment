import pytest
import allure
from allure_commons.types import AttachmentType
import sys
sys.path.append('tests')


@pytest.fixture(scope='class')
def selenium_setup_and_teardown(request):
    from selenium import webdriver

    browser = "chrome"  # "firefox" "safari" "internet explorer"
    if "chrome" in browser:
        options = webdriver.ChromeOptions()
        capabilities = options.to_capabilities()
    elif "firefox" in browser:
        options = webdriver.FirefoxOptions()
        capabilities = options.to_capabilities()
    elif "safari" in browser:
        capabilities = {'browserName': 'safari', 'platform': 'MAC'}
    web_driver = webdriver.Remote(desired_capabilities=capabilities, command_executor='http://localhost:4444/wd/hub')

    # web_driver.set_window_position(-3435, -518)
    # web_driver.set_window_size(1210, 1418)

    request.cls.driver = web_driver
    yield
    web_driver.quit()


@pytest.mark.hookwrapper
def pytest_runtest_makereport(item, call):
    outcome = yield
    report = outcome.get_result()
    request = getattr(item, '_request', None)
    driver_defined = hasattr(request.cls, 'driver')
    if driver_defined:
        driver = request.cls.driver
    else:
        driver = None
    xfail = hasattr(report, 'wasxfail')
    failure = (report.skipped and xfail) or (report.failed and not xfail)
    when = item.config.getini('selenium_capture_debug').lower()
    capture_debug = when == 'always' or (when == 'failure' and failure)
    if capture_debug:
        exclude = item.config.getini('selenium_exclude_debug').lower()
        if driver is not None:
            if 'screenshot' not in exclude:
                allure.attach(driver.get_screenshot_as_png(), name = "Screenshot", attachment_type = AttachmentType.PNG)
